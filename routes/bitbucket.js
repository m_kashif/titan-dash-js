
const express = require('express')
const router  = express.Router()

const util    = require('../utilities/util')
const {TITAN_TRIGGER, AUTH_TOKEN} = require('../config/keys')

router.post('/invoke', async (req, res) => {
	
	const token = req.query.token

	if(token !== AUTH_TOKEN)
		return res.json({err: 'Invalid token'})

	if(req.body.approval)
		return res.json({err: 'Skipping titan build for approval requests'})

	let {pullrequest, repository, actor} = req.body
	try {
		actor  		= JSON.parse(actor)
		pullrequest = JSON.parse(pullrequest)
		repository  = JSON.parse(repository)
	} catch(err) {
		console.log(err)
	}

	const dstBranch = pullrequest.destination.branch.name
	const srcBranch = pullrequest.source.branch.name

	if( (repository.name === 'hyper-configs' && dstBranch !== 'master') ||
		(repository.name !== 'hyper-configs') && !dstBranch.match(/(beta|release)/))
		res.json({msg: `Skipping titan build for branch ${srcBranch}`})

	else {
		try {
			
			const payload   = await util.getAttachments(pullrequest, repository)
			const slackRes  = await util.sendSlackMessage(payload, 'postMessage')
			if(slackRes.ok) {
				payload.text = payload.text.replace('triggering', 'triggered')
				payload.ts   = slackRes.ts
			}

			let url = dstBranch.match(/test/) ? TITAN_TRIGGER.test : TITAN_TRIGGER.local

			const titanRes 	= await util.triggerTitan(url, {pullrequest, repository, actor, slack_ts: payload.ts})
			if(titanRes.status === 200) {
				await util.sendSlackMessage(payload, 'chatUpdate')
				res.json({msg: 'triggered a build'})
			} else res.json({msg: 'Could not trigger titan', err: titanRes})
		} catch(err) {
			console.log(err)
			res.json({msg: 'Check console', err})
		}
	}
})

module.exports = router
