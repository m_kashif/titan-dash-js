
const express = require('express')
const db      = require('../utilities/db')
const router  = express.Router()

const {AUTH_TOKEN} = require('../config/keys')
const { log, checkBuildNumberExists,
		retrieveRequestPayload } = require('../utilities/util')

router.get('/rebuild/:BuildNumber', async (req, res) => {

	const BuildNumber = req.params.BuildNumber
	const shouldBuild = await checkBuildNumberExists(BuildNumber)

	if(shouldBuild === true) {
		const payload = await retrieveRequestPayload(BuildNumber)
		triggerBuild(payload)

		res.json({msg: `re-building #${BuildNumber}`})
	}
	else return res.json({msg: 'Cannot rebuild.', err: shouldBuild})
})

router.post('/rebuild', (req, res) => {

	const body = req.body
	const {repository, PR} = body
})

router.post('/db/update/:token', async (req, res) => {

	const token = req.params.token
	if(token !== AUTH_TOKEN)
		return res.json({msg: 'Invalid token'})
		
	const payload = req.body
	try {
		const buildRes = await db.updateBuild(payload)
		const projectRes = await db.updateProject(buildRes)
		res.json({msg: 'updated successfully', build: buildRes, project: projectRes})
	} catch(err) {
		log('post-build', 'db-update-err')
		console.log(err)
	}
})

module.exports = router