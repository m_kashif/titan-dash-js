
const slackPost = require('slack-webhook-send')

const {SLACK_API_TOKEN} = require('../config/keys')

const getAttachments = async (pr, repo, channel = 'C016YS8EQ7K') => {
	const dstBranch  = pr.destination.branch.name
	const srcBranch  = pr.source.branch.name
	const prLink     = pr.links.self.href
	const prState    = pr.state
	const repoName   = repo.name
	const repoLink   = repo.links.html.href

	const dstFields   = `<https://bitbucket.org/juspay/${repoName}/branch/${dstBranch}|${dstBranch}>`
	const srcFields   = `<https://bitbucket.org/juspay/${repoName}/branch/${srcBranch}|${srcBranch}>`
	const repository  = `\`<${repoLink}|${repo.name}>\``
	const pullRequest = `\`<${prLink}|${repo.name}/${pr.id}>\` - ${prState}`
	const build       = repoName.match(/hyper-configs/) ? 'config' : 'asset'
	const type		  = prState === 'OPEN' ? '[DEV]'
						: dstBranch.match(/beta/) ? '[BETA]'
						: dstBranch.match(/release/) ? '[RELEASE]'
						: '[PROD]'
	const header  = `${type} triggering ${build} build`
	const fields  = ['Repository', repository, 'Pull Request', pullRequest,
					'Source Branch', srcFields, 'Destination Branch', dstFields,
					'Review PR', prLink]
	const buttons = ['Review PR']
	const payload  = await slackPost.getPayload(fields, buttons, 'trigger', channel, header)

	return payload
}

const sendSlackMessage = (payload, sendMethod) => new Promise(async function(resolve, reject) {
	try {
		const res = await slackPost.slackSend(payload, sendMethod, SLACK_API_TOKEN)
		resolve(res)
	} catch(err) {
		reject(err)
	}
})

module.exports = {
	getAttachments,
	sendSlackMessage
}