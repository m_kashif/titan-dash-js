
const axios = require('axios')
const fs    = require('fs')
const path = require('path')

const {	getAttachments,
		sendSlackMessage } = require('./slack')

const {	checkBuildNumberExists,
		retrieveRequestPayload } = require('./bitbucket')

const downloadReport = url => new Promise(async function(resolve, reject) {
	axios({
		method: "get",
		url,
		responseType: "stream"
	}).then(res => {
		const filename = url.replace(/.*\//, '').replace(/\?.*/, '')
		const filePath = path.resolve(__dirname, filename)
		const fileStream = fs.createWriteStream(filePath)
		res.data.pipe(fileStream)
		fileStream.on('finish', () => resolve(filePath))
	}).catch(err => reject(err))
})

const deleteFiles = filesArr => filesArr.map(file => {
	try {
		fs.unlinkSync(file)
	} catch(err) {
		console.error(err)
	}
})

const getBuffer = filePath => new Promise(function(resolve, reject) {

	const readStream = fs.createReadStream(filePath, {highWaterMark: 64000})
	const data = [];
	readStream.on('data', chunk => data.push(chunk))
	readStream.on('end', () => resolve(Buffer.concat(data)))
	readStream.on('error', err => reject(err))
})

const getBuildPayload = async payload => {

	const pipeline = payload.pipeline
	const filesToDelete = []
	let buildPayload = {}

	if(pipeline === 'asset') {
		buildPayload = {
			app: {
				name: payload.app_name,
				id: payload.app_id,
				version: payload.app_version
			},
			scope: payload.release_scope,
			s3_path: {
				android: payload.android_asset_path,
			},
			size: {
				js: payload.jsSize,
				jsa: payload.jsaSize,
			},
			report: {}
		}

		try {
			const androidReportLink = payload.android_report_link
			let savedPath = await downloadReport(androidReportLink)
			buildPayload.report.android = await getBuffer(savedPath)
			filesToDelete.push(savedPath)
		} catch(err) {
			log('android-report', 'error retrieveing buffer')
			console.log(err)
		}

		if(payload.ios_asset_path) {
			buildPayload.s3_path.ios = payload.ios_asset_path
			try {
				const iosReportLink = payload.ios_report_link
				savedPath = await downloadReport(iosReportLink)
				buildPayload.report.ios = await getBuffer(savedPath)
				filesToDelete.push(savedPath)
			} catch(err) {
				log('ios-report', 'error retrieveing buffer')
				console.log(err)
			}
		}

		deleteFiles(filesToDelete)
	} else if(pipeline === 'config') {
		const configs = payload.configs
		buildPayload = []
		configs.map(config => {
			buildPayload.push({
				app: {
					name: config.app_name,
					id: config.app_id,
					filename: config.filename,
					version: config.version
				},
				scope: config.release_scope,
				s3_path: {
					android: config.android_config_path
				}
			})
			if(payload.ios_path) buildPayload.s3_path.ios = payload.ios_config_path
		})
	} else if(pipeline === 'android-sdk') {

	} else if(pipeline === 'ios-sdk') {

	}

	return buildPayload
}

const log = (stage, str, id = null) => console.log(`[${stage}] ${str.replace('{{id}}', id)}`)

const triggerTitan = (url, payload) => new Promise(function(resolve, reject) {
	axios.post(url, payload)
		.then(res => resolve(res))
		.catch(err => reject(err))
})

module.exports = {
	triggerTitan,
	log,
	// bitbucket
	checkBuildNumberExists,
	retrieveRequestPayload,
	// db
	getBuildPayload,
	// slack
	getAttachments,
	sendSlackMessage,
}
