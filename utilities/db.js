
const {getBuildPayload} = require('../utilities/util')

const Build   = require('../models/Build')
const Project = require('../models/Project')

const updateProject = payload => new Promise(function(resolve, reject) {

		const repository = payload.build.bitbucket_payload.repository
		const repo = {
			name: repository.name,
			url: repository.links.html.href
		}

		Project.findOne({'repository.name': repo.name})
			.then(project => {
				if(!project) {
					const newProject = new Project({repository: repo})
					return newProject.save()
				}
				return project
			})
			.then(project => {
				project.builds.push({build: payload._id})
				return project.save()
			})
			.then(updatedProject => resolve(updatedProject))
			.catch(err => reject(err))
	})

const updateBuild = payload => new Promise(async function(resolve, reject) {

		let {repository, pullrequest, actor} = payload
		let {build_number, rebuild, dev, slack_ts, changelog, pipeline} = payload
		const buildPayload = await getBuildPayload(payload)

		try {
			actor = JSON.parse(actor)
			pullrequest = JSON.parse(pullrequest)
			repository = JSON.parse(repository)
		} catch(err) {}
		const bitbucket_payload = { repository, pullrequest, actor }
		
		Build.findOne({build_number: build_number})
			.then(build => {
				if(build) { build_number += '_latest' }
				
				const newBuild = new Build({
					build_number,
					build: {
						rebuild,
						dev,
						bitbucket_payload,
						slack_ts,
						changelog: changelog.split('~'),
						pipeline
					}
				})

				if(pipeline === 'asset') 			newBuild.build.asset = buildPayload
				else if(pipeline === 'config') 		newBuild.build.config = buildPayload
				else if(pipeline === 'android-sdk') newBuild.build.android = buildPayload
				else if(pipeline === 'ios-sdk') 	newBuild.build.ios = buildPayload

				return newBuild.save()
			})
			.then(res => resolve(res))
			.catch(err => reject(err))
	})

module.exports = {
	updateBuild,
	updateProject
}
