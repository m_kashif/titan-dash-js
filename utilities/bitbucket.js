
const axios = require('axios')
const {BITBUCKET} = require('../config/keys')

const endpoints = {
	"bitbucket": {
		"getPRLink"   : (repo, PR) 	=> `https://api.bitbucket.org/2.0/repositories/juspay/${repo}/pullrequests/${PR}`,
		"getRepoLink" : (repo) 		=> `https://api.bitbucket.org/2.0/repositories/juspay/${repo}`
	}
}

const checkBuildNumberExists = buildNumber => new Promise((resolve, reject) => {
})

const retrieveRequestPayload = buildNumber => new Promise(async (resolve, reject) => {

	const prPayload = axios.get(endpoints.bitbucket.getPRLink,
								{Authorization: {username: BITBUCKET.user, password: BITBUCKET.app_password}})
})

module.exports = {
	checkBuildNumberExists,
	retrieveRequestPayload
}