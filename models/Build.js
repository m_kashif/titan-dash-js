
const mongoose = require('mongoose')

const BuildSchema = {
	build_number: {
		type: String
	},
	build: {
		rebuild: {
			type: Number,
			default: 0
		},
		dev: {
			type: Boolean,
			default: true
		},
		bitbucket_payload: {
			type: Object
		},
		slack_ts: {
			type: String
		},
		changelog: [{
			type: String
		}],
		pipeline: {
			type: String
		},
		asset: {
			app: {
				name: {
					type: String
				},
				id: {
					type: String
				},
				version: {
					type: String
				}
			},
			report: {
				android: {
					type: Buffer
				},
				ios: {
					type: Buffer
				}
			},
			scope: {
				type: String,
				default: 'beta'
			},
			s3_path: {
				android: {
					type: String
				},
				ios: {
					type: String
				}
			},
			size: {
				js: {
					type: Number
				},
				jsa: {
					type: Number
				}
			},
		},
		config: [{
			app: {
				name: {
					type: String
				},
				id: {
					type: String
				},
				version: {
					type: String
				},
				filename: {
					type: String
				}
			},
			scope: {
				type: String,
				default: 'beta'
			},
			s3_path: {
				android: {
					type: String
				},
				ios: {
					type: String
				}
			}
		}],
		android: {

		},
		ios: {

		}
	}
}

module.exports = Build = mongoose.model('build', BuildSchema)