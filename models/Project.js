
const mongoose = require('mongoose')
const Schema   = mongoose.Schema

const ProjectSchema = {
	repository: {
		name: {
			type: String
		},
		url: {
			type: String
		}
	},
	builds: [{
		build: {
        	type: Schema.Types.ObjectId,
        	ref: 'build'
		},
		date: {
			type: Date,
			default: new Date()
		}
	}]
}

module.exports = Project = mongoose.model('project', ProjectSchema)