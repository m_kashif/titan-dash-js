module.exports = {
	AUTH_TOKEN    	 : process.env.AUTH_TOKEN,
	BITBUCKET: {
		user		 : process.env.BITBUCKET_USER,
		app_password : process.env.BITBUCKET_APP_PASSWORD
	},
	MONGO_URI		 : process.env.MONGO_URI,
	SLACK_API_TOKEN	 : process.env.SLACK_API_TOKEN,
	TITAN_TRIGGER: {
		local : process.env.TITAN_TRIGGER_LOCAL,
		prod  : process.env.TITAN_TRIGGER_PROD,
		test  : process.env.TITAN_TRIGGER_TEST
	},
}