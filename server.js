
const bodyParser = require('body-parser')
const express    = require('express')
const mongoose   = require('mongoose')

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// connect to database
mongoose.set('useFindAndModify', false)
const URI = require('./config/keys.js').MONGO_URI
mongoose
  	.connect(URI, {useNewUrlParser: true, useUnifiedTopology: true})
  	.then(() => console.log('Connected to mongoDB.'))
  	.catch(err => console.log(err))

// routes
const bitbucketWebhook = require('./routes/bitbucket')
const titanAPI = require('./routes/titan')

app.use('/generic-webhook-trigger', bitbucketWebhook)
app.use('/titan', titanAPI)

// host
const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Listening on port ${port}.`))
